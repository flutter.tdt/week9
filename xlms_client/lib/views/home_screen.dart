import 'package:flutter/material.dart';
import 'package:xlms_authentication/xlms_authentication.dart';

class HomeScreen extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return _HomeScreenState();
  }

}

class _HomeScreenState extends State<StatefulWidget> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text('Home screen'),),
      body: Text('Welcome to Flutter App.'),
      floatingActionButton: FloatingActionButton(
        onPressed: () {
          _processLogin();
        },
      ),
    );
  }

  void _processLogin() async {
    const sakaiUrl = 'https://xlms.myworkspace.vn';
    XlmsService sakaiService = XlmsService(xlmsUrl: sakaiUrl);

    var response = await sakaiService.authenticate('demo', 'demo');

    print(response.statusCode);
    print(response.body);
  }
}
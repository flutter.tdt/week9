import 'package:flutter_test/flutter_test.dart';

import 'package:datetime_util/datetime_util.dart';

void main() {
  test('Add some working days to a datetime.', () {

    var now = DateTime.now();
    var curDay = DateTime(now.year, now.month, now.day);

    expect(DateUtil.addWorkingDay(curDay, 4), DateUtil.parse('2022-4-17', 'yyyy-MM-dd'));

  });
}
